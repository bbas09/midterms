class User < ActiveRecord::Base 
	has_many :car_log

	validates :username,
		presence: true,
		uniqueness: { case_sensitivity: false},
		length: {minimum: 3, maximum: 5}

	validates :email 
		presence: true
		length: {maximum: 105}
		uniqueness: {case_sensitivity: false}
end 