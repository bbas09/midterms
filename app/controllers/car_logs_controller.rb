class CarLogsController < ApplicationController
  before_action :set_car, only: [:edit, :update, :show, :delete]
  # before_action :require_user, except: [:index, :show]
  before_action :require_same_user, only: [:edit, :update, :delete]
  def require_same_user
    if current_user != @car.user and !current_user.admin? 
      flash[:notice] = " "
      redirect_to root_path
    end
  end 
  def new
  	@car_logs = CarLog.new
  end

  def create
  	@car_logs = CarLog.new(car_logs_params)
    # @car_logs.user = User.last #temporary
    @car.user = current_user
  	if @car_logs.save
  		flash[:notice] = "Create New Car"


  		redirect_to car_logs_new_path(@car_logs)
  	else
  		render 'new'

  	end
  end

  def cars
  	@car_logs = CarLog.last
  end



  def destroy
    @car_logs.destroy
    flash[:notice] = 'tite'
    redirect_to car_logs(@car)
  end





  private
  def car_logs_params
  	params.require(:car_log).permit(:plate_number, :time_in, :time_out, :color, :brand)
  end

  def set_car
    @car_logs
  end 
end
