class SessionsController < ApplicationController
	def new
		render 'new'
	end 

	def create
		#debugger 
		#finds email 
		user = User.find_by(email: params[:session][:email].downcase)
		#user = User.find_by(email: )

		#if the user exists, itll render
		if user && user.authenticate(params[:session][:password])
			session[:user_id] = user.id 
			flash[:success] = "Logged in successfully"
			redirect_to user_path(user)
		else 
			flash.now[:danger] = "There was something wrong with your login"
			render 'new'
		end
	end 

	def destroy
		session[:user_id] = nil
		flash[:notice] = "Logged out"
		redirect_to root_path
	end 
end
