class CreateCarLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :car_logs do |t|
      t.string :plate_number
      

      t.timestamps
    end
  end
end
