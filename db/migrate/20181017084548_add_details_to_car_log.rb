class AddDetailsToCarLog < ActiveRecord::Migration[5.2]
  def change
    add_column :car_logs, :time_in, :datetime
    add_column :car_logs, :time_out, :datetime
    add_column :car_logs, :color, :string
    add_column :car_logs, :brand, :string
  end
end
