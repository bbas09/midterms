Rails.application.routes.draw do
  get 'infos/me'
  get 'car_logs/new', to: 'car_logs#new'
  get 'car_logs/cars', to: 'car_logs#cars'
  root to: 'welcome#index'

  resources :car_logs

  get 'login', to: 'sessions#new'
  post 'login', to: 'session#create'
  delete 'logout', to: 'sessions#destroy'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
